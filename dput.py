#!/usr/bin/python

import sys

import osc.cmdln as cmdln

sys.path.insert(0, '/usr/share/osc-plugins-dput')
import osc_plugins_dput
sys.path.remove('/usr/share/osc-plugins-dput')

@cmdln.option('--maintained-in-git', action='store_true',
              help='add MAINTAINED_IN_GIT.txt')
def do_dput(self, subcmd, opts, proj_name, dsc_file):
    """
    ${cmd_name}: Automate the process of submiting a debian package to the OBS server.

    It expects a PROJECT_NAME and a .dsc, source .changes file, or
    source + binary .changes file. If used with a source + binary .changes
    file, the binaries will not be used.

    Usage:
        osc [global options] dput PROJECT_NAME PACKAGE.dsc
        osc [global options] dput PROJECT_NAME PACKAGE_source.changes
    """

    # in older versions of osc, this ends up in the class namespace (!)
    if hasattr(self, "osc_plugins_dput"):
        osc_plugins_dput = self.osc_plugins_dput

    osc_plugins_dput.do_dput(self, subcmd, opts, proj_name, dsc_file)
